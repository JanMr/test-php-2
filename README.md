# Test assignement 2

## Rules:
  - everyone must do the assgnement by them self
  - code must be documented
  - write README for usage

## Assignement
  - Test Data: https://admin.b2b-carmarket.com//test/project
  - Create CLI command that takes "url to data|filename","output data type" as params
  - CLI command should determen what kind of data type is the input data.
  - Create generic output to "output data type" - obligatory supported types are: excel, xml, json, csv
  - Output shuld be generated based on inputed data. 
  
### MUST HAVE:
  - Write all tests needed to have whole process covered - at least 80% code coverage
  - Use Interfaces and abstracts to harden the arhitecture.
  
### SHOULD HAVE:
 - GUI that support same process as CLI (file upload/input for link) and selector of output types - the output should be a file
  
